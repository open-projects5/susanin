package com.example.susaninproject.Main;

import android.content.Context;
import android.media.MediaPlayer;

public class MusicPlayer {
    private static MediaPlayer mediaPlayer;
    private static boolean isPlaying = false;

    public static void playMusic(Context context, int resourceId) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(context, resourceId);
            mediaPlayer.setLooping(true);
        }

        if (!isPlaying) {
            mediaPlayer.start();
            isPlaying = true;
        }
    }

    public static void stopMusic() {
        if (mediaPlayer != null && isPlaying) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            isPlaying = false;
        }
    }
}
