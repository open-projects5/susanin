package com.example.susaninproject.Main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.susaninproject.R;

public class Dialog extends AppCompatActivity {
    private Button personButton, questionButtonText, nextDialogButton;
    private View layout;
    private static String[] sentences = {};
    private byte currentSentence = 0;
    private static byte dialogOrder = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog);

        layout = findViewById(R.id.dialog_layout);
        personButton = findViewById(R.id.susaninButton);
        questionButtonText = findViewById(R.id.questionButtonText);
        nextDialogButton = findViewById(R.id.nextDialogButton);
        nextDialogButton.setOnClickListener(v -> nextSentence());

        setDialogFromArray();
        showSentence();
    }

    public static boolean passedAllDialogs() {
        return (dialogOrder > dialogs.length - 1);
    }

    public boolean passedAllSentences() {
        return (currentSentence > sentences.length - 1);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setDialogFromArray() {
        if (!passedAllDialogs()) {
            sentences = dialogs[dialogOrder];
            if(backgroundsAndCharacters[dialogOrder].length<2) return;

            layout.setBackground(getDrawable(backgroundsAndCharacters[dialogOrder][0]) );
            personButton.setBackground(getDrawable(backgroundsAndCharacters[dialogOrder][1]));
        }
    }

    public static void skip() {
        dialogOrder++;
    }

    public static boolean isEmpty() {
        return passedAllDialogs() || dialogs[dialogOrder].length == 0;
    }

    public void nextSentence() {
        currentSentence++;
        if (passedAllSentences()) {
            skip();
            finish();
        } else {
            showSentence();
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void showSentence() {
        byte n = (byte) Math.max(0, (Math.min(currentSentence, sentences.length - 1)));
        if (sentences[n] != null) questionButtonText.setText(sentences[n]);


    }

    @Override
    public void finish() {
        if (!passedAllSentences()) {
            currentSentence = (byte) Math.max(0, currentSentence - 1);
            showSentence();
        } else {
            finishAndRemoveTask();
            overridePendingTransition(0, 0);
        }
    }

    public static void reset() {
        dialogOrder = 0;
        sentences = new String[]{};
    }

    private static final String[][] dialogs = {
            {},
            {},
            {},
            {"Оказывается, это следы недавно пробегавшей кошки",
                    "Давай спросим у нее, не видела ли она Бобика?" },
            {},
            {},
            {"Быть может, Бобик гулял на площади?"}
    };
    private static final int[][] backgroundsAndCharacters = {
            {},
            {},
            {},
            {R.drawable.back7, R.drawable.susanin},
            {},
            {},
            {R.drawable.back1, R.drawable.susanin}

    };
    // нужен текст для каждого диалога(и картинки) , пустые пропускаются


}


