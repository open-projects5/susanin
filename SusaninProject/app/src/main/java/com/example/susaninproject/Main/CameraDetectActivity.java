package com.example.susaninproject.Main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.susaninproject.R;

import org.opencv.android.CameraActivity;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class CameraDetectActivity extends CameraActivity implements CameraBridgeViewBase.CvCameraViewListener2 {
    private static final String TAG = "ObjectDetection::Activity";
    private static final Scalar BOX_COLOR = new Scalar(0, 255, 0);
    public static boolean tf = false;
    private Mat mRgba;
    private CascadeClassifier mCascadeClassifier;
    private MatOfRect mDetections;
    private String[] names_cascade = {"cascade", "cascade_firetower"};
    private String current_name_cascade;
    private int min_n;
    private int current_cascade;
    private int countQuestionPassedReceived;
    private Point point_text = new Point(50, 50);
    private Scalar scalar_text = new Scalar(0, 255, 0);

    private CameraBridgeViewBase mOpenCvCameraView;

    public CameraDetectActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);

        countQuestionPassedReceived = getIntent().getIntExtra("countQuestionPassedReceived", 4);

        if (OpenCVLoader.initLocal()) {
            Log.i(TAG, "OpenCV loaded successfully");
            initClassifier();
        } else {
            Log.e(TAG, "OpenCV initialization failed!");
            (Toast.makeText(this, "OpenCV initialization failed!", Toast.LENGTH_LONG)).show();
            return;
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_camera);

        mOpenCvCameraView = findViewById(R.id.cameraView);
        mOpenCvCameraView.setVisibility(CameraBridgeViewBase.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    private void initClassifier() {
        try {
            switch (countQuestionPassedReceived) {
                case 5: {min_n = 90; current_name_cascade = names_cascade[0]; current_cascade = R.raw.cascade; break;}
                case 7: {min_n = 40; current_name_cascade = names_cascade[1]; current_cascade = R.raw.cascade_firetower; break;}
            }

            InputStream is = getResources().openRawResource(current_cascade);
            File cascadeDir = getDir(current_name_cascade, Context.MODE_PRIVATE);
            File cascadeFile = new File(cascadeDir, String.format("%s.xml", current_name_cascade));
            FileOutputStream os = new FileOutputStream(cascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            mCascadeClassifier = new CascadeClassifier(cascadeFile.getAbsolutePath());
            if (mCascadeClassifier.empty()) {
                Log.e(TAG, "Failed to load cascade classifier");
                mCascadeClassifier = null;
            } else {
                Log.i(TAG, "Loaded cascade classifier from " + cascadeFile.getAbsolutePath());
            }

            cascadeDir.delete();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.enableView();
    }

    @Override
    protected List<? extends CameraBridgeViewBase> getCameraViewList() {
        return Collections.singletonList(mOpenCvCameraView);
    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat();
        mDetections = new MatOfRect();
    }

    public void onCameraViewStopped() {
        mRgba.release();
        mDetections.release();
    }

    @Override
    public void onBackPressed() {
        // Остановить камеру
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }

        // Закрыть приложение или вернуться к предыдущему экрану
        super.onBackPressed();
    }

    private long objectDetectedTime = 0;
    private boolean objectDetected = false;

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();

        if (mCascadeClassifier != null) {
            mCascadeClassifier.detectMultiScale(mRgba, mDetections, 1.1, min_n, 0, new Size(75, 100), new Size());

            for (Rect rect : mDetections.toArray()) {
                Imgproc.rectangle(mRgba, rect.tl(), rect.br(), BOX_COLOR, 2);

                if (!objectDetected) {
                    objectDetectedTime = System.currentTimeMillis();
                    objectDetected = true;
                }

                if (System.currentTimeMillis() - objectDetectedTime <= 3000) {
                    Imgproc.putText(mRgba, "Object Detected", point_text, Imgproc.FONT_HERSHEY_COMPLEX, 1.0, scalar_text, 2);
                } else {
                    Imgproc.putText(mRgba, "", point_text, Imgproc.FONT_HERSHEY_COMPLEX, 1.0, scalar_text, 2);
                    objectDetected = false;
                }

                if (mDetections.toArray().length >= 1 && (System.currentTimeMillis() - objectDetectedTime >= 3000)) {
                    tf = true;
                    Intent intent = new Intent(this, SecondActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                }
            }
        } else {
            Imgproc.putText(mRgba, "Classifier not loaded", point_text, Imgproc.FONT_HERSHEY_COMPLEX, 1.0, scalar_text, 2);
        }

        if (mDetections.toArray().length == 0) {
            Imgproc.putText(mRgba, "No detect " + countQuestionPassedReceived, point_text, Imgproc.FONT_HERSHEY_COMPLEX, 1.0, scalar_text, 2);
            objectDetected = false;
        }

        return mRgba;
    }
}