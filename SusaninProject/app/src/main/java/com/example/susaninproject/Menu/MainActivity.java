package com.example.susaninproject.Menu;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.susaninproject.R;
import com.example.susaninproject.Main.SecondActivity;

public class MainActivity extends AppCompatActivity {
    public Button startButton, saveButton, optionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = findViewById(R.id.buttonStart);
        saveButton = findViewById(R.id.buttonSave);
        optionsButton = findViewById(R.id.buttonOptions);

        // Анимация появления кнопок
        startButton.setAlpha(0f);
        saveButton.setAlpha(0f);
        optionsButton.setAlpha(0f);

        startButton.animate().alpha(1f).translationY(0f).setDuration(500).setStartDelay(200).start();
        saveButton.animate().alpha(1f).translationY(0f).setDuration(500).setStartDelay(600).start();
        optionsButton.animate().alpha(1f).translationY(0f).setDuration(500).setStartDelay(800).start();

        // Анимация нажатия кнопок
        startButton.setOnClickListener(view -> {
            startButton.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200).start();
            new Handler().postDelayed(() -> {
                startButton.animate().scaleX(1f).scaleY(1f).setDuration(200).start();
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);
            }, 200);
        });

        saveButton.setOnClickListener(view -> {
            saveButton.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200).start();
            new Handler().postDelayed(() -> {
                saveButton.animate().scaleX(1f).scaleY(1f).setDuration(200).start();
                // Код для сохранения данных
            }, 200);
        });

        optionsButton.setOnClickListener(view -> {
            optionsButton.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200).start();
            new Handler().postDelayed(() -> {
                optionsButton.animate().scaleX(1f).scaleY(1f).setDuration(200).start();
                // Код для открытия экрана настроек
            }, 200);
        });
    }
}