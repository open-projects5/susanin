package com.example.susaninproject.Final;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.susaninproject.Main.MusicPlayer;
import com.example.susaninproject.Menu.MainActivity;
import com.example.susaninproject.R;

public class FinalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);
    }

    public void restart(View view) {
        // перезапуск игры
        MusicPlayer.stopMusic();
        Intent intent = new Intent(FinalActivity.this, MainActivity.class);
        startActivity(intent);
    }
}