package com.example.susaninproject.Main;

import android.content.Context;
import android.view.View;

import com.example.susaninproject.R;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.runtime.image.ImageProvider;

import java.util.ArrayList;

interface QuestionListener {
    void onQuestionChanged(GameQuestion.Question question);
    void nextQuestion(View view);
    void onQuestionFinish();
    void onQuestionArrivedToPoint();
}

public class GameQuestion {
    private QuestionListener listener;
    private int count = 0;
    private static Question[] questions;
    private ArrayList<Question> passed = new ArrayList<>();

    {
        questions = new Question[]{
                new Question("У меня странное чувство, что нужно идти к памятнику, который отмечен на карте\n",
                        0,
                        0,
                        R.drawable.back2,
                        new double[] {57.766968, 40.924771},
                        "Памятник находится в центре города",
                        "Пропала любимая собака Сусанина!\n" +
                                "Иван Сусанин проснулся утром и обнаружил, что его верный пёс Бобик исчез! Он быстро оделся и выбежал на улицу, чтобы начать поиски.\n" +
                                "Сусанин обошёл весь свой двор, но Бобика нигде не было. Тогда он решил спросить у соседей, не видели ли они его собаку."
                ),

                new Question("Сусанин говорит, что у него пропала собака.\n" + // набережная
                        "Он уверен, что последний раз видел ее на набережной.\n" +
                        "Вам предстоит дойти до набережной, чтобы начать поиски..",
                0,
                R.drawable.susanin,
                        R.drawable.back5,
                new double[] {57.764903, 40.921369},
                        "Спуститесь вниз к Волге по Молочной горе",
                        null
                ),

                new Question("\"В парке зеленом, там, где река шумит,\n" + // островский
                        "  Известный писатель свое время проводил.\n" +
                        "  В книгах своих он жизнь города описал,\n" +
                        "  Где же это место? Скорее найди след.\"",
                        0,
                        R.drawable.susanin,
                        R.drawable.back6,
                        new double[]{57.761780, 40.928790},
                        "Встаньте лицом к Волге и развернитесь налево и идите вперед пока не увидите беседку на возвышении",
                        null
                ),

                new QuestionPawPrints("О, смотри - следы!",  // кошка
                        R.drawable.question1,
                        R.drawable.susanin,
                        R.drawable.back7,
                        new double[] {57.767473, 40.925857},
                        PawPrints.Park,
                        "Выясните, куда ведут следы",
                        null
                ),

                new QuestionOPENCV("\"На статуе великого князя загадка скрыта,\n" +
                        "Фотографию возьми\n" +
                        "И путь держи туда скорей\"",
                        R.raw.statue,
                        R.drawable.cat,
                        R.drawable.back5,
                        new double[]{57.766586, 40.929325},
                        "Встаньте лицом к Сусанинской площади, поверните направо и идите, пока не увидите памятник",
                        "А теперь сфотографируй мне памятник Долгорукого",
                        null
                ),

                new Question( "  \"Династия, что спасена была мною,\n" +
                        "  Имя ей знакомо, в истории светло.\n" +
                        "  Найди ее следы, узнай истину\"",
                        0,
                        R.drawable.dolg,
                        R.drawable.back5,
                        new double[]{57.769598, 40.929805},
                        "На карте отмечено примерное местоположение следующей контрольной точки (музея Романовых)",
                        null
                ),

                new QuestionOPENCV("\"Великое прошлое, династия великая,\n" +
                        "В музее их историю можно узнать.\n" +
                        "Каланча тебя ждет, там тайна раскроется твоя.\"",
                        R.raw.kalancha,
                        R.drawable.susanin,
                        R.drawable.back1,
                        new double[]{57.768801, 40.925978},
                        "Спуститесь вниз до площади Сусанинской и поверните направо, идите вперед, пока не увидите Каланчу",
                        "А теперь надо найти и сфотографировать пожарную каланчу",
                        null
                ),
                new QuestionPawPrints("Мне кажется, или я вижу следы?\n",  // на площади
                        R.drawable.question1,
                        R.drawable.susanin,
                        R.drawable.back1,
                        new double[] {57.76781627613382,40.92699558960293},
                        PawPrints.Square,
                        "Выясните, куда ведут следы",
                        null
                )
        };
    }

    public static Question[] getQuestions() {
        return questions;
    }

    public ArrayList<Question> getPassed() {
        return passed;
    }

    public void setListener(QuestionListener listener) {
        this.listener = listener;
    }

    public void generateNextQuestion() {
        if(count == questions.length) {
            listener.onQuestionFinish();
            return;
        }

        if (listener != null) {
            if(count != 0) {
                passed.add(questions[count-1]);
            }
            listener.onQuestionChanged(questions[count++]);
        }
    }

    public void generateOpenCVQuest() {
        listener.onQuestionArrivedToPoint();
    }

    public class Question {
        private String question;
        private int pictureItem;
        private int picturePerson;
        private int background;
        private String description;
        private String textSvitok;
        private double[] coord;
        public Question(String question) {
            this.question = question;
        }

        public Question(
                String question,
                int pictureItem,
                int picturePerson,
                int background,
                double[] coord,
                String description,
                String textSvitok
        ) {
            this(question);
            this.pictureItem = pictureItem;
            this.picturePerson = picturePerson;
            this.background = background;
            this.coord = coord;
            this.description = description;
            this.textSvitok = textSvitok;
        }

        public String getQuestion() {
            return question;
        }
        public int getPictureItem() {
            return pictureItem;
        }
        public int getBackground() {
            return background;
        }
        public int getPicturePerson() {
            return picturePerson;
        }
        public String getTextSvitok() { return textSvitok; }
        public double[] getCoord() {
            return coord;
        }
        public String getDescription() {return description; }
    }

    class QuestionOPENCV extends Question{
        private String textArrivedToPoint;
        private boolean isPhotoCorrect;
        public QuestionOPENCV(
                String question,
                int pictureItem,
                int picturePerson,
                int background,
                double[] coord,
                String description,
                String textArrivedToPoint,
                String textSvitok
        )
        {
            super(question, pictureItem, picturePerson, background, coord, description, textSvitok);
            this.textArrivedToPoint = textArrivedToPoint;
        }

        public String getTextArrivedToPoint() {
            return textArrivedToPoint;
        }

        public void setIsPhotoCorrect(boolean isPhotoCorrect) {
            this.isPhotoCorrect = isPhotoCorrect;
        }

        public boolean getIsPhotoCorrect() {
            return isPhotoCorrect;
        }
    }

    class QuestionPawPrints extends Question{

        private double [][] pawPrintsCoords;

        public QuestionPawPrints(String question, int pictureItem, int picturePerson, int background, double[] coord, double[][] pawPrintsCoords, String description, String textSvitok) {
            super(question, pictureItem, picturePerson, background, coord, description, textSvitok);
            this.pawPrintsCoords = pawPrintsCoords;
        }

        public void addPlacemarks(MapObjectCollection mapObjectCollection, Context ctxt){

            for (double[] coord: pawPrintsCoords) {
                mapObjectCollection.addPlacemark(
                        new Point(coord[0], coord[1]),
                        ImageProvider.fromResource(ctxt, R.drawable.ic_paw)
                );
            }
        }

    }
}


