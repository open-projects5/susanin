package com.example.susaninproject.Main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.susaninproject.Final.FinalActivity;
import com.example.susaninproject.Menu.MainActivity;
import com.example.susaninproject.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKit;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Circle;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.mapkit.user_location.UserLocationLayer;
import com.yandex.runtime.image.ImageProvider;

public class SecondActivity extends AppCompatActivity implements QuestionListener {
    private static boolean isMapVisible = false;
    private Button personButton, questionButtonText, artifactButton, descriptionButtonInfo, questionButtonPanel, userLocationButton, textSvitokButton, cameraButton;
    private Button[] currentQuestButtons;
    private GameQuestion gameQuestion;
    private View[] viewQuestionField;
    private MapHelper mapHelper;
    private MapObjectCollection mapObjects;
    private GameQuestion.Question currentQuestion;
    private int countQuestionPassed = 0;
    private double[] currentUserPostion;

    private Handler handler;
    private Runnable runnable;
    private static final double[] KOSTROMALATTITUDE = new double[] {57.767921, 40.926896};

    static { MapKitFactory.setApiKey("93fd6dae-bda9-4e90-94f0-e02fe3c29246"); }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapKitFactory.initialize(this);
        setContentView(R.layout.activity_second);

        // Воспроизведение музыки
        MusicPlayer.playMusic(this, R.raw.fable_s);

        handler = new Handler(Looper.getMainLooper());
        runnable = new Runnable() {
            @Override
            public void run() {
                // Код, который будет выполняться в цикле
                // Активируйте метод, используя полученное значение
                if (CameraDetectActivity.tf)  {
                    CameraDetectActivity.tf = false;
                    nextQuestion(null);

                }
                handler.postDelayed(this, 3000); // Повторять каждую секунду
            }
        };

        // Запуск цикла
        handler.post(runnable);

        // инициализируем переменные
        mapHelper = new MapHelper();
        mapHelper.mapView = findViewById(R.id.mapview);
        personButton = findViewById(R.id.susaninButton);
        artifactButton = findViewById(R.id.artifactButton);
        questionButtonText = findViewById(R.id.questionButtonText);
        cameraButton = findViewById(R.id.cameraButton);
        textSvitokButton = findViewById(R.id.svitokButton);
        textSvitokButton.setVisibility(View.INVISIBLE);
        userLocationButton = findViewById(R.id.userLocationButton);
        userLocationButton.setEnabled(false);
        // отступ текста 10 процентов
        questionButtonText.post(new Runnable() {
            @Override
            public void run() {
                // Теперь кнопка отрисована, и мы можем получить ее ширину
                int width = questionButtonText.getWidth();
                int paddingLeft = (int) (width * 0.1);
                int paddingRight = (int) (width * 0.1);
                questionButtonText.setPadding(paddingLeft, 0, paddingRight, 0);
            }
        });
        questionButtonPanel = findViewById(R.id.clueButton);
        descriptionButtonInfo = findViewById(R.id.descriptionButtonInfo);
        descriptionButtonInfo.setVisibility(View.INVISIBLE);
        questionButtonPanel.setEnabled(false);
        viewQuestionField = new View[] {
                personButton,
                questionButtonText,
        };
        gameQuestion = new GameQuestion();
        gameQuestion.setListener(this);

        currentQuestButtons = new Button[] {
                findViewById(R.id.quest1),
                findViewById(R.id.quest2),
                findViewById(R.id.quest3),
                findViewById(R.id.quest4),
                findViewById(R.id.quest5),
                findViewById(R.id.quest6),
                findViewById(R.id.quest7),
                findViewById(R.id.quest8)
        };
        for(Button btn: currentQuestButtons) {
            btn.setVisibility(View.INVISIBLE);
        }

        mapHelper.moveCameraToLocation(KOSTROMALATTITUDE[0], KOSTROMALATTITUDE[1]);
        mapHelper.hideYandexMap();
        nextQuestion(null);
        getLocationUser();
        isUserAtPoint();
        //сбрасываем счетчик диалогов
        Dialog.reset();
    }

    public void showCharacter(View view) {
        android.view.animation.Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        view.startAnimation(fadeIn);
    }

    @Override
    public void onQuestionChanged(GameQuestion.Question myQuestion) {
        // переход на следующий вопрос
        questionButtonText.setText(myQuestion.getQuestion());
        View secondActivityLayout = findViewById(R.id.activity_second);
        if (myQuestion instanceof GameQuestion.QuestionOPENCV)
            cameraButton.setEnabled(true);
        else
            cameraButton.setEnabled(false);
        if(currentQuestion instanceof GameQuestion.QuestionPawPrints)
            mapHelper.mapView.getMap().getMapObjects().clear();
        if(myQuestion instanceof GameQuestion.QuestionPawPrints)
             ((GameQuestion.QuestionPawPrints) myQuestion).addPlacemarks(mapHelper.mapView.getMap().getMapObjects(), mapHelper.mapView.getContext());
        if(myQuestion.getTextSvitok() != null) {
            textSvitokButton.setVisibility(View.VISIBLE);
            textSvitokButton.setText(myQuestion.getTextSvitok());
        } else
            textSvitokButton.setVisibility(View.INVISIBLE);
        secondActivityLayout.setBackgroundResource(myQuestion.getBackground()); // Используем ресурс изображения
        if(myQuestion.getPicturePerson() != 0)
            personButton.setBackground(getResources().getDrawable(myQuestion.getPicturePerson()));

        if(myQuestion.getPictureItem() != 0 && !isMapVisible) {
            artifactButton.setVisibility(View.VISIBLE);
            artifactButton.setBackground(getResources().getDrawable(myQuestion.getPictureItem()));
        } else
            artifactButton.setVisibility(View.INVISIBLE);
        if (mapHelper.mapView.getVisibility() != View.VISIBLE)
            showCharacter(personButton);

        double[] data = myQuestion.getCoord();
        mapHelper.paintArea(data[0], data[1], USER_LOCATION_RADIUS);
        currentQuestion = myQuestion;
    }

    @Override
    public void onQuestionFinish() {
        // завершение игры
        Intent intent = new Intent(SecondActivity.this, FinalActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    @Override
    public void onQuestionArrivedToPoint() {
        GameQuestion.QuestionOPENCV question = (GameQuestion.QuestionOPENCV) currentQuestion;
        if(question.getPictureItem() != 0 && !isMapVisible) {
            artifactButton.setVisibility(View.VISIBLE);
            artifactButton.setBackground(getResources().getDrawable(question.getPictureItem()));
        } else {
            artifactButton.setVisibility(View.INVISIBLE);
        }
        questionButtonText.setText(question.getTextArrivedToPoint());
    }
    public class MapHelper {
        // класс для работы с картой
        private MapView mapView;
        private UserLocationLayer userLocationLayer;

        private void toggleMapVisibility() {
            // обрабатываем нажатие самолётик - карта
            if (isMapVisible) {
                hideYandexMap();
                if(currentQuestion.getPictureItem() != 0) {
                    artifactButton.setVisibility(View.VISIBLE);
                }
                if(currentQuestion.getTextSvitok() != null) {
                    textSvitokButton.setVisibility(View.VISIBLE);
                }
                userLocationButton.setEnabled(false);
                questionButtonPanel.setEnabled(false);
            } else {
                showYandexMap();
                artifactButton.setVisibility(View.INVISIBLE);
                textSvitokButton.setVisibility(View.INVISIBLE);
                userLocationButton.setEnabled(true);
                questionButtonPanel.setEnabled(true);
            }
            isMapVisible = !isMapVisible;
            descriptionButtonInfo.setVisibility(View.INVISIBLE);
        }

        private void hideYandexMap() {
            // сворачиваем карту
            if (mapView != null) {
                mapView.setVisibility(View.GONE);
            }
        }

        private void showYandexMap() {
            // открываем карту
            if (mapView != null) {
                mapView.setVisibility(View.VISIBLE);
            }
        }

        private void paintArea(double x, double y, int radius) {
            if (mapView != null) {
                mapView.getMap().getMapObjects().addCircle(new Circle(new Point(x, y), radius), Color.BLUE, 4f, Color.TRANSPARENT);

                for (GameQuestion.Question que: gameQuestion.getPassed()) {
                    double[] coord = que.getCoord();
                    mapView.getMap().getMapObjects().addCircle(new Circle(new Point(coord[0], coord[1]), radius), Color.RED, 4f, Color.TRANSPARENT);
                }
            }
        }

        private void moveCameraToLocation(double latitude, double longitude) {
            mapHelper.mapView.getMap().move(
                    new CameraPosition(new Point(
                            latitude,
                            longitude),
                            15.0f,
                            0.0f,
                            0.0f
                    ),
                    new Animation(Animation.Type.SMOOTH, 1f),
                    null
            );
        }
    }

    private MapKit mapKit;
    private int USER_LOCATION_RADIUS = 30;

    private void getLocationUser() {
        // инициализируем мапкит и получаем метку пользователя
        requestLocationPermission();
        mapKit = MapKitFactory.getInstance();
        mapHelper.userLocationLayer = mapKit.createUserLocationLayer(mapHelper.mapView.getMapWindow());
        mapHelper.userLocationLayer.setVisible(true);
    }

    private void requestLocationPermission() {
        // получаем метку пользователя
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                            {
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION},
                    0
            );
        }
    }

    private void isUserAtPoint() {
        // асинхронная проверка местоположения пользователя
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);

        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback()  {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);

                    if (locationResult != null && locationResult.getLastLocation() != null) {
                        double userLatitude = locationResult.getLastLocation().getLatitude();
                        double userLongitude = locationResult.getLastLocation().getLongitude();
                        double[] coord = currentQuestion.getCoord();
                        double latitudePosition = coord[0];
                        double longitudePosition = coord[1];
                        currentUserPostion = new double[] {userLatitude, userLongitude};

                        if (isNearPosition(userLatitude, userLongitude, latitudePosition, longitudePosition)) {
                            nextDialogAndQuestion(null);
                            //nextQuestion(null);
                            toggleMap(null);
                        }
                    }
                }
            }, Looper.getMainLooper());
        }
    }

    private boolean isNearPosition(
            double latitudeUser,
            double longitudeUser,
            double latitudePosition,
            double longitudePosition
    )
    {
        // Проверка расстояния
        double distance = distanceBetweenPoints(latitudeUser, longitudeUser, latitudePosition, longitudePosition);
        return distance <= USER_LOCATION_RADIUS;
    }

    private double distanceBetweenPoints(
            double lat1,
            double lon1,
            double lat2,
            double lon2
    )
    {
        // Используем формулу Хаверсина для расчета расстояния на сфере
        double R = 6371e3;
        double lat1Rad = Math.toRadians(lat1);
        double lon1Rad = Math.toRadians(lon1);
        double lat2Rad = Math.toRadians(lat2);
        double lon2Rad = Math.toRadians(lon2);

        double dLat = lat2Rad - lat1Rad;
        double dLon = lon2Rad - lon1Rad;

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1Rad) * Math.cos(lat2Rad) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }

    public void openCamera(View view) {
            // получаем доступ к камере
            Intent intent = new Intent(SecondActivity.this, CameraDetectActivity.class);
            intent.putExtra("countQuestionPassedReceived", countQuestionPassed);
            startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapHelper.mapView.onStop();
        MapKitFactory.getInstance().onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapHelper.mapView.onStart();
        MapKitFactory.getInstance().onStart();
    }

    public void toggleMap(View view) {
        // открываем или закрываем карту
        for (View v : viewQuestionField) {
            v.setVisibility(isMapVisible ? View.VISIBLE : View.INVISIBLE);
        }
        mapHelper.toggleMapVisibility();
    }

    boolean doShowDescription = false;
    public void descriptionInfo(View view) {
        // возвращаемся в меню
        if(!doShowDescription) {
            descriptionButtonInfo.setVisibility(View.VISIBLE);
        } else {
            descriptionButtonInfo.setVisibility(View.INVISIBLE);
        }
        descriptionButtonInfo.setText(currentQuestion.getDescription());
        doShowDescription = !doShowDescription;
    }

    public void backToMenu(View view) {
        MusicPlayer.stopMusic();
        // возвращаемся в меню
        Intent intent = new Intent(SecondActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void findUserPosition(View view) {
        // перемещаемся на координаты пользователя
        if (currentUserPostion != null) {
            mapHelper.moveCameraToLocation(
                    currentUserPostion[0],
                    currentUserPostion[1]
            );
        }
    }

    public void nextQuestion(View view) {
        // следующий вопрос
//        if(currentQuestion instanceof GameQuestion.QuestionOPENCV) {
//            GameQuestion.QuestionOPENCV questionOPENCV =  (GameQuestion.QuestionOPENCV) currentQuestion;
//            if(!questionOPENCV.getIsPhotoCorrect()) {
//                gameQuestion.generateOpenCVQuest();
//                return;
//            }
//        }
        gameQuestion.generateNextQuestion();
        descriptionButtonInfo.setVisibility(View.INVISIBLE);
        doShowDescription = false;
        if(countQuestionPassed < currentQuestButtons.length)
            currentQuestButtons[countQuestionPassed++].setVisibility(View.VISIBLE);
    }



    public void ShowDialog(){
        Intent intent = new Intent(SecondActivity.this, Dialog.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void nextDialogAndQuestion(View view){
        if (!Dialog.isEmpty()){
            ShowDialog();
            handler.postDelayed(() -> nextQuestion(view), 500);
        }
        else {
            Dialog.skip();
            nextQuestion(view);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Остановка воспроизведения музыки
        MusicPlayer.stopMusic();
    }

}